import os
import cfg
import psutil


THIS_PID = os.getpid()


def ongoingInstallProcs():
    ongoing = []
    for process in psutil.process_iter():
        if process.username() == cfg.SUDO_USER:
            if process.name() == 'python':
                cmdline = process.cmdline()
                if 'install.py' in ' '.join(cmdline):
                    ongoing.append(process)
    return ongoing


def tree(topPID):
    parent = psutil.Process(topPID)
    for kid in parent.children(recursive=True):
        yield kid

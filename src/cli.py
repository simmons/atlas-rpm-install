import argparse
import cfg
from collections import namedtuple
import datetime
import legal
import re


Args = namedtuple('Args', 'branch platform release datetime')


def parse(validate=True):
    """Parse the command line args, validate and return them."""
    args = parseArgs()
    if validate:
        validateArgs(args)
    branch, platform, release = args.release.split('/')
    return Args(branch, platform, release, args.nicosdt)


def parseArgs():
    desc = 'Install script for nightly RPMs on CVMFS'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-r', dest='release', required=True)
    parser.add_argument('-n', dest='nicosdt', required=True)
    return parser.parse_args()


def validateArgs(args):
    """Validate the command line arguments returned
    from parse_args() function. This function will
    raise an exception if problems are found.
    """
    _validateRelease(args.release)
    _validateNicosStamp(args.nicosdt)


def _validateRelease(release):
    """Validate the value passed on the command line
    with the -r argument. Expect a string of the form:
    <branch>/<platform>/<release>
    Raise a ValueError if necessary.
    """
    if not release.count('/') == 2:
        m = '-r argument expected of the form:\n'
        m += '<branch>/<platform>/<release>'
        raise ValueError(m)

    _, platform, releaseName = release.split('/')
    _validatePlatform(platform)
    _validateReleaseName(releaseName)
    return True


def _validatePlatform(platform):
    try:
        binary, os_, compiler, build = platform.split('-')
        ok = True
    except ValueError:
        ok = False
    else:
        ok = ok and binary in legal.BINARY
        ok = ok and os_ in legal.OS
        ok = ok and compiler in legal.COMPILER
        ok = ok and build in legal.BUILD

    if not ok:
        m = '-r argument expected of the form:\n'
        m += '<branch>/<platform>/<release>\n'
        m += 'where <platform> is something like x86_64-slc6-gcc49-opt\n'
        m += 'Illegal platform: {0}'.format(platform)
        raise ValueError(m)

    return True


def _validateReleaseName(release):
    if not re.compile('^rel_\d$').match(release):
        m = '-r argument expected of the form:\n'
        m += '<branch>/<platform>/<release>\n'
        m += 'where <release> is something like rel_3\n'
        m += 'Illegal release: {0}'.format(release)
        raise ValueError(m)
    return True


def _validateNicosStamp(nicosdt):
    """Check the validity of the value passed in on the
    command line -n argument. Expect a string like
    2016-12-12T1023. Raise a ValueError if necessary.
    """
    regex = re.compile('^(\d{4})-(\d\d)-(\d\d)T(\d\d)(\d\d)$')
    match = regex.match(nicosdt)

    err = '-n argument expected of the form:\n'
    err += 'YYYY-MM-DDTHHmm, e.g.2016-08-16T1234'

    if not match:
        raise ValueError(err)

    year, month, day, hour, mins = match.groups()
    if not _isValidDatetime(year, month, day, hour, mins):
        raise ValueError(err)

    return True


def _isValidDatetime(year, month, day, hour, mins):
    """Construct if possible a datetime.datetime instance
    from the parameters. Failure to do so will be due to
    illegal dates or times such as 31st Feb 2017.
    Return a boolean indicating outcome.
    """
    try:
        datetime.datetime(int(year),
                          int(month),
                          int(day),
                          int(hour),
                          int(mins))
    except ValueError:
        return False
    return True

import re


def get():
    """Get the list of known install errors."""
    return [
        Error('Requires:\s+(.*)',
              lambda matches: 'Missing deps ({0})'.format('\n'.join(matches)),
              matchall=True),

        Error('^(http.*?/repomd.xml).*?Operation too slow',
              lambda match: 'Server timeout ({0})'.format(match)),

        Error('Package does not match intended download',
              lambda match: ('Repo out of sync, run "ayum '
                             '--enablerepo=atlas-offline-nightly clean all"')),

        Error('ValueError: prefix given, but package is not relocatable',
              lambda match: 'Prefix given, but package not relocatable')
    ]


class Error(object):
    def __init__(self, patt, callback, matchall=False):
        self.patt = patt
        self.signature = re.compile(patt)
        self.matchall = matchall
        self.matches = []
        self.callback = callback

    def match(self, line):
        sre = self.signature.match(line)
        if sre:
            matches = sre.groups()
            matches = [sre.group()] if not matches else matches
            self.matches.append(matches)
        return sre is not None

    def __str__(self):
        """Call the callback, passing in a list of matches.
        Each item in the matches list is itself a list of one or more
        items that matched a given line.
        """
        return self.callback(self.matches)

"""
Simple application monitor.

At various points in the execution, the application calls one of
the below methods. This updates the in-memory status dict, and then
writes it out as JSON to a file in cfg.MONITOR_DIR called:
<branch>__<platform>__<release>__<buildStart>__<installStart>.json
This JSON file can then be used by a 3rd-party script to display
the state of this and previous installations (this application
provides one itself: status.py).
Format of the status dict is (epoch = integer Unix epoch):
{
    'process': {
        'start': epoch,
        'stop': (None | epoch),
        'states': [{}, {}, ...]
    },
    'install': [{}, {}, ....],
    'timings': [{}, {}, ....]
}
The process/states list contains a dict per state traversed,
and in the order traversed. For example:
{
    'name': 'run',
    'when': epoch
}
or
{
    'name': 'err',
    'when': epoch,
    'info': 'Nightly already installed'
}
The install and timings list of dicts have a length equal
to the number of installs. In general will be 1, but for
full releases where we split into offline and hlt, the length
will be 2. The install dicts are of the form:
{
    'start': epoch,
    'stop': (epoch|None),
    'status': (run|fail|ok),
    'info': ('' | <failReason>)
    'rpms': [list of rpms installed]
}
The timings dicts are of the form:
{
    'install': [intNumbSeconds1, intNumbSeconds2, ..],
    'publish': (None | intNumbSeconds)
}
where timings/install is a list in the same order as the
install key list of dicts.
"""
import cfg
import cli
import json
import log
import os
import time
from util import createDir, createFile, die, START_EPOCH


def constructMonFileName():
    args = cli.parse(validate=False)
    monfile = '__'.join([args.branch,
                         args.platform,
                         args.release,
                         args.datetime,
                         str(START_EPOCH)])
    return monfile + '.json'


class State:
    def __init__(self, name, when=None, info=None):
        self.name = name
        self.when = int(time.time()) if when is None else when
        self.info = '' if info is None else info


class Status(object):
    status = {
        'process': {
            'states':[]
        },
        'install': [],
        'timings': {'install':[], 'publish': None}
    }

    def __init__(self, monitorDir):
        self.createMonitorDir(monitorDir)
        monitorFileName = constructMonFileName()
        self.monitorFile = os.path.join(monitorDir, monitorFileName)

    def createMonitorDir(self, mondir):
        if not createDir(mondir):
            m = '{0}: unable to create monitoring dir'.format(mondir)
            die(m)

    def write(self):
        with open(self.monitorFile, 'w') as f:
            f.write(json.dumps(self.status))


class Process(Status):
    def __init__(self, monitorDir):
        Status.__init__(self, monitorDir)

    def appendState(self, state):
        self.status['process']['states'].append(state.__dict__)
        self.write()

    def waiting(self, contents):
        self.appendState(State('pend', info=contents))

    def ongoing(self):
        self.appendState(State('run'))

    def error(self, reason=''):
        self.appendState(State('err', info=reason))

    def start(self):
        self.status['process']['start'] = START_EPOCH
        self.write()

    def stop(self):
        self.status['process']['stop'] = int(time.time())
        self.write()

    def log(self, logpath):
        # Store the path to the log file for this install
        self.status['process']['log'] = logpath

    def installdir(self, basedir):
        self.status['process']['installdir'] = basedir
        

class Install(Status):
    def __init__(self, monitorDir):
        Status.__init__(self, monitorDir)

    def start(self, rpms):
        self.status['install'].append({
            'start': int(time.time()),
            'rpms': rpms,
            'stop': None,
            'state': 'run'
        })
        self.write()

    def failed(self, reason=''):
        d = self.status['install'][-1]
        d['stop'] = int(time.time())
        d['state'] = 'fail'
        d['info'] = reason
        self.status['install'][-1] = d
        self.write()

    def succeeded(self):
        d = self.status['install'][-1]
        d['stop'] = int(time.time())
        d['state'] = 'ok'
        self.status['install'][-1] = d
        self.write()


class Timings(Status):
    def __init__(self, monitorDir):
        Status.__init__(self, monitorDir)

    def install(self, seconds):
        self.status['timings']['install'].append(seconds)
        self.write()

    def publish(self, seconds):
        self.status['timings']['publish'] = seconds
        self.write()


# Until monitor.init() is called, just blackhole any calls
# to monitor.process/install/timings. This is useful for
# unit testing.
class devnull(object):
    def __getattr__(self, name):
        def blackhole(*args, **kws):
            pass
        return blackhole

process = devnull()
install = devnull()
timings = devnull()


def init(monitorDir=cfg.MONITOR_DIR):
    global process
    global install
    global timings
    process = Process(monitorDir)
    install = Install(monitorDir)
    timings = Timings(monitorDir)

#! /bin/bash

# ----------------------------------------------------
# - Install script for ATLAS nightly RPMs
#
# 2016-09: Brinick Simmons - initial version
# ----------------------------------------------------
usage(){
    echo 'Usage:'
    echo '   ./install.sh -r <release> -n <nicos_datetime>'
    echo 'where:'
    echo '   <release> is the release to be installed'
    echo '   <nicos_datetime> is the build start datetime'
    echo 'Example:'
    echo '   ./install.sh -r 22.0.X/x86_64-slc6-gcc49-opt/rel_4 -n 2016-08-23T1234'
}

# ---------------------------------------------------
# Signal handling for shutdown/clean up
# ---------------------------------------------------
_shutdown(){
    echo 'Caught signal, forwarding TERM to install.py child process';
    kill -TERM $installProcPID;
}

trap _shutdown SIGTERM SIGINT;

# ---------------------------------------------------
# Construct needed directory paths
# ---------------------------------------------------
THIS_DIR=$(dirname "${BASH_SOURCE[0]}");
VIRTUALENV_DIR="$THIS_DIR/../install.venv";
VIRTUALENV_DIR=$(readlink -e $VIRTUALENV_DIR);  # abs path
SRC_DIR="$THIS_DIR/src";

# ---------------------------------------------------
# Get the arguments to pass to the install.py script
# ---------------------------------------------------
while getopts "r:n:h" arg; do
    case "${arg}" in
        r)
            release=${OPTARG};
            ;;
        n)
            nicosdt=${OPTARG};
            ;;
        h)
            usage
            exit 0;
            ;;
        *)
            usage
            exit 1;
            ;;
    esac;
done;
shift $((OPTIND-1));

# Check we provided both arguments to the script
if [ -z $release ] || [ -z $nicosdt ];then
    usage
    exit 1;
fi

# ---------------------------------------------------
# Functions
# ---------------------------------------------------
banner(){
    echo "******************************************";
    echo "  To shut this process down cleanly:      ";
    echo "      kill -15 $$                         ";
    echo "******************************************";
}

virtualenv_activate(){
    if [ ! -d $VIRTUALENV_DIR ];then
         echo "Inexistant: $VIRTUALENV_DIR";
         exit 1;
    fi

    echo "Activating: $VIRTUALENV_DIR";
    source "$VIRTUALENV_DIR/bin/activate";
}

install(){
    cd $SRC_DIR;
    echo "Running $SRC_DIR/install.py";
    python install.py -r $release -n $nicosdt &
    installProcPID=$!;
    wait $installProcPID;
}

virtualenv_deactivate(){
    echo "Deactivating: $VIRTUALENV_DIR";
    deactivate;
    echo "Done";
}

main(){
    banner;
    virtualenv_activate;
    install;
    virtualenv_deactivate;
}
# ---------------------------------------------------
# Run...
# ---------------------------------------------------
main;

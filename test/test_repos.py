from collections import namedtuple
from cfg import RPM_REPO_NIGHTLY_BASE
import exc
import os

import mock
import repos
import pytest


# - Helper functions ----
def fakeURLResponse_ok():
    R = namedtuple('Response', 'ok text status_code')
    return R(True,
             '<a href="ying.rpm">ying</a><a href="yang.text">yang</a>',
             200)


def fakeURLResponse_fail():
    R = namedtuple('Response', 'ok text status_code')
    return R(False, 'All is not well', 404)


def test_repos_instance_str():
    name = 'test-repo'
    fullname = 'My test repo'
    url = 'https://fakeurl.com/fake/'
    r = repos.Repo(name, fullname, url)
    expected = """[test-repo]
name=My test repo
baseurl=https://fakeurl.com/fake/
enabled=1
"""
    assert str(r) == expected


def test_nightly_repo_url():
    args = namedtuple('Args', 'branch platform release')
    run = '22.0.X/x86_64-slc6-gcc49-opt/rel_4'
    a = args(*run.split('/'))
    expected = os.path.join(RPM_REPO_NIGHTLY_BASE, run)
    sortedURL = repos.nightlyRepoURL(a, sorted=True)
    unsortedURL = repos.nightlyRepoURL(a, sorted=False)
    assert unsortedURL == expected
    assert sortedURL == expected + '/?C=M;O=A'


def test_parse_rpms_page():
    page = '<a href="ying.rpm">ying</a><a href="yang.text">yang</a>'
    links = repos.parseRPMsPage(page)
    assert type(links) == type([])
    assert len(links) == 1
    assert links[0] == 'ying.rpm'


def test_get_remote_repos():
    args = namedtuple('Args', 'branch platform release datetime')
    run = '22.0.X/x86_64-slc6-gcc49-opt/rel_4'
    a = args(*run.split('/'), datetime='2016-12-25T1234')
    installdir = os.getcwd()  # irrelevant what it actually is
    repos.getRemoteRepos(a, installdir)


@mock.patch('requests.get')
def test_list_nightly_rpms(fake_get):
    fake_get.return_value = fakeURLResponse_ok()
    url = 'http://cern.ch'
    links = repos.listNightlyRPMs(url)
    assert len(links) == 1
    assert links[0] == 'ying.rpm'


def test_list_nightly_rpms_inexistant_repo():
    inexistantURL = 'https://cern.ch/atlas-software-dist-eos/inexistant/'
    with pytest.raises(exc.RPMListRetrievalFailure):
        repos.listNightlyRPMs(inexistantURL)


def test_list_nightly_rpms_repo_timeout():
    nonRoutableURL = 'http://10.255.255.1'
    with pytest.raises(exc.RPMListRetrievalFailure):
        repos.listNightlyRPMs(nonRoutableURL, timeout=0.1)

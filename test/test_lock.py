import pytest
import lock


@pytest.fixture
def setup(tmpdir):
    return {'testdir': str(tmpdir)}


def test_lock_create(setup):
    lock.LOCK_DIR = setup['testdir']

import sys

import mock
import monitor
import os
import pytest
import util
import sys


@pytest.fixture
def setup(tmpdir):
    sys.argv[1:] = [
        '-r',
        '22.0.X/x86_64-slc6-gcc49-opt/rel_4',
        '-n',
        '2016-08-23T1234'
    ]
    monitor.init(str(tmpdir))
    return {'testdir': str(tmpdir)}


def test_monitor_file(setup):
    monfileName = ('22.0.X__x86_64-slc6-gcc49-opt__rel_4'
                   '__2016-08-23T1234__'
                   '{0}.json'.format(util.START_EPOCH))
    monfile = os.path.join(str(setup['testdir']), monfileName)
    assert monitor.process.monitorFile == monfile


def test_add_ongoing_status(setup):
    monitor.process.ongoing()
    assert len(monitor.process.status['process']['states']) == 1
    state = monitor.process.status['process']['states'][0]
    assert isinstance(state, dict)
    assert len(state) == 3
    assert sorted(state.keys()) == ['info', 'name', 'when']
    assert state['info'] == ''
    assert state['name'] == 'run'


def test_monitor_start(setup):
    assert not os.path.exists(monitor.process.monitorFile)
    monitor.process.start()
    monfileName = ('22.0.X__x86_64-slc6-gcc49-opt__rel_4'
                   '__2016-08-23T1234__'
                   '{0}.json'.format(util.START_EPOCH))
    monfile = os.path.join(setup['testdir'], monfileName)
    assert os.path.exists(monitor.process.monitorFile)

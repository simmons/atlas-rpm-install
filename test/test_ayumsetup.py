import sys
sys.argv[1:] = ['-r',
                '22.0.X/x86_64-slc6-gcc49-opt/rel_4',
                '-n',
                '2016-08-23T1234']

import ayumsetup
from collections import namedtuple
from exc import AyumDownloadError
import mock
import pytest

@pytest.fixture
def setup(tmpdir):
    sys.argv[1:] = [
        '-r',
        '22.0.X/x86_64-slc6-gcc49-opt/rel_4',
        '-n',
        '2016-08-23T1234'
    ]
    monitor.init(str(tmpdir))
    return {'testdir': str(tmpdir)}


def fakeShell_fail():
    S = namedtuple('S', 'out ok exitcode cmd')
    return S(['fail'], False, 1, 'whatever')


def fakeShell_ok():
    S = namedtuple('S', 'out ok exitcode cmd')
    return S(['yay'], True, 0, 'whatever')


@mock.patch('ayumsetup.log')
@mock.patch('ayumsetup.shellCmd')
def test_ayum_download_fail_raises(fake_shell, fake_log):
    fake_shell.return_value = fakeShell_fail()
    with pytest.raises(AyumDownloadError):
        installdir = '/dev/null'
        ayumdir = '/dev/null'
        ayumsetup.Ayum(ayumdir, installdir).download()


@mock.patch('ayumsetup.log')
@mock.patch('ayumsetup.shellCmd')
def test_ayum_download(fake_shell, fake_log):
    fake_shell.return_value = fakeShell_ok()
    installdir = '/dev/null'
    ayumdir = '/dev/null'
    ayumsetup.Ayum(ayumdir, installdir).download()
    fake_log.info.assert_called_once()
    fake_log.info_assert_called_with('yay')

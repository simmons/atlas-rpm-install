from collections import defaultdict, namedtuple
from exc import URLRetrieveFail
import mock
import os
import pytest
import util
import requests


# - Helper functions ----
def fakeURLResponse_ok():
    R = namedtuple('Response', 'ok text status_code')
    return R(True, 'All is well', 200)


def fakeURLResponse_fail():
    R = namedtuple('Response', 'ok text status_code')
    return R(False, 'All is not well', 404)


def fakePopen():
    class Proc(object):
        def __init__(self):
            self.stdout = open(os.devnull, 'w')
            self.stderr = open(os.devnull, 'w')

        def poll(self):
            self.returncode = 0
            return self.returncode

        def communicate(self):
            out = 'out text'
            err = 'err text'
            return (out, err)

    return Proc()

# -------------------------------------------------

@mock.patch('util.log')
@mock.patch('requests.get')
def test_get_url_no_timeout(fake_get, fake_log):
    fake_get.return_value = fakeURLResponse_ok()
    statusCode, response = util.getURL('http://cern.ch/atlas')
    assert response.ok
    assert response.text == 'All is well'
    fake_log.info.assert_called_once()
    fake_log.info.assert_called_with('All is well')


@mock.patch('util.log')
def test_get_url_timeout_raises(fake_log):
    nonRoutableURL = 'http://10.255.255.1'
    with pytest.raises(requests.exceptions.Timeout):
        util.getURL(nonRoutableURL, timeout=0.1)


@mock.patch('util.log')
@mock.patch('subprocess.Popen')
def test_shell_cmd(fake_popen, fake_log):
    fake_popen.return_value = fakePopen()
    response = util.shellCmd('ls')
    assert response.ok
    assert response.cmd == 'ls'
    assert response.out == ['out text']
    assert fake_log.debug.call_count == 2
    # fake_log.debug.assert_called_once()
    # fake_log.debug.assert_called_with('Executing shell cmd:\nls')

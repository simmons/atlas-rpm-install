#!  /bin/bash

no_pytest(){
    echo "ERROR: no pytest module found, it is required to run tests";
    echo "Get it by running:";
    echo "  pip install pytest"
    exit 1;
}

ROOT=$(dirname "${BASH_SOURCE[0]}");
export PYTHONPATH=$ROOT/src:$PYTHONPATH;
cd $ROOT;
python -c 'import pytest' >& /dev/null || no_pytest;
python -m pytest -s
